# Instalación Jenkins

1. docker network create jenkins

2. docker volume create jenkins-docker-certs

3. docker volume create jenkins-data

4. docker container run --rm --detach --privileged --network jenkins --network-alias docker --env DOCKER_TLS_CERTDIR=/certs --volume jenkins-docker-certs:/certs/client --volume jenkins-data:/var/jenkins_home --publish 2376:2376 docker:dind

5. docker container run --rm --detach --network jenkins --env DOCKER_HOST=tcp://docker:2376 --env DOCKER_CERT_PATH=/certs/client --env DOCKER_TLS_VERIFY=1 -p 8066:8080 -p 50066:50000 --volume jenkins_prueba:/var/jenkins_home --volume jenkins-docker-certs:/certs/client:ro jenkinsci/blueocean


# ------------------------------------------------------------------------------------------------------------------------


# Commit
git add . && git commit -m "docker build" && git push -u origin master